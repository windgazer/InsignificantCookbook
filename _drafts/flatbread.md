---
title: Home-made Flatbread
layout: recipe
tags:
    - bread
    - base
thumb:
    link: https://www.flickr.com/photos/windgazer/3187604841
    src: https://farm4.staticflickr.com/3475/3187604841_53c14a3470_o.jpg
---

Flatbread is somewhat generic in it's use and type of bread. What I'm looking for are some
different uses too. For one, there's the simple type of flatbread that can be used as a
wrap, important to me, I like wrapping stuff. The second is one that must be easy to make
on camping equipment and preferably with ingredients that are easy to keep while on the
road. That type of flatbread is intended for use with sauces, spreads, cheeses and the
likes. Potentially a make-shift pizza-bottom.

### Wrappable flatbread

This recipe will be based upon one I found recently at [RecipeTin Eats][1]. Mostly I'll
want to figure out a metric conversion for the recipe and hopefully smaller portions.
This due to the fact that I live alone and would like to have it so that I can start
cooking meals that I can finish the same evening.

#### No metrics

So, while I started experimenting I found out that the concept of using 'cups' is actually
quite useful, although rather unscientific. The reason for this is that it turns out, it's
just easy to add a few cups of such and so, then add twice that amount of some other
ingredient. Certainly much easier to remember that way. So, my rather unscientific recipe
came down to:

- 50g of butter
- 3/4 cup of milk (when added to butter)
- 1 cup of flour
- a pinch of salt (or other herbs)

#### Cup of milk when added to butter?

So, the weird part of this recipe is the milk and butter. I literally throw the butter
into the cup and then fill it with milk till about 3/4 full. When using a big cup, add
more butter before putting in the milk... The great thing is, the only metric part of this
recipe is now butter and any size of a cup will do. Can't have too much butter in this
recipe, as far as I've found, so just err on the side of caution if you think your cup is
a little too big.

### Flatbread for the road

I found a recipe from [No Meat Athlete][2] that pretty much reads as something that could
be made on the road.


[1]: http://www.recipetineats.com/easy-soft-flatbread-yeast/
[2]: http://www.nomeatathlete.com/vegan-flatbread/
