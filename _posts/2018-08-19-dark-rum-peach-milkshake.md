---
title: Dark Rum and Peach Milkshake
layout: recipe
tags:
    - drink
    - desert
thumb:
    link: https://www.flickr.com/photos/windgazer/44141043011
    src: https://farm2.staticflickr.com/1861/44141043011_924d13d027_c.jpg
---

After having managed to get the right mix for a gloriously chocolate flavoured
rum milkshake, I found myself with some left-over peach-syrup. I decided to
throw two shots of Plantation Rum, pineaple, into the syrup and put in the
fridge. Fast forward a few days and I have a good friend visiting and I know
she's going to like a peachy milkshake!

Here's the mix I came up with:

- 2 shots of Plantation Rum (Pineapple)
- 4 shots of Peach syrup
- 4 large scoops of ice-cream

Here's the recipe:

1. Put the Rum and Syrup in the fridge for two days (optional)
2. Put the Rum and Syrup in the blender
3. Add the ice-cream
4. Run the blender until you have a smooth mixture

The aim is the end up with roughly 250ml worth of milkshake, which is a large
glass full, add more ice-cream if needed.

[![Dark Rum and Peach Milkshake][imgI]][I]

For the milkshakes in the picture I used the entire can of peach parts. Taking
two of the peach parts out for decoration purposes and doubling all the other
ingredients.

- 1 small can of peach parts on syrup
- 4 shots of Plantation Rum (Pineapple)
- 8 large scoops of ice-cream

Here's the recipe:

1. Put the Rum and Peach together in the fridge for two days (optional)
2. Keep two peach parts to the side.
3. Put rum and peach mixture in the blender
4. Add the ice-cream
5. Run the blender until you get a smooth mix
6. Make roughly a 3 quarter cut into the peach about 2 thirds from the side and
    place it as garnish over the edge of the glass.

[I]: https://www.flickr.com/photos/windgazer/44141032231
[imgI]: https://farm2.staticflickr.com/1862/44141032231_e69f08d45e.jpg
