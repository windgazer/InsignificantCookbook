---
title: Easy does it Pesto
layout: recipe
tags:
    - sauce
thumb:
    link: https://www.flickr.com/photos/windgazer/6197166519
    src: https://farm7.staticflickr.com/6014/6197166519_8b0199e258_o.jpg
---
Let's start off by saying, this is something I've wanted to do for a long time now, make
my own fresh pasta. Word-of-mouth says this is a simple matter and with practice doesn't
have to take much longer than using pre-packed pasta...

Let's see if I can find this as a post on my pages?
