---
title: About
---

# About the insignificant cookbook

This is my cookbook. Well, more a book of recipes. Sure, a lot of the source
material may well be based on some other recipe, but the intention is that I've
done these myself and should help me repeat my success.

Where the recipe is solidly based on a single original source, with my own twist
or two, I'll link back to the original. Where I've mixed a whole bunch of them,
I'm considering the recipe to be an original.

Enjoy the recipes and cheers,

  Martin 'Windgazer' Reurings
