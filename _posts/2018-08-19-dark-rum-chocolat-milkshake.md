---
title: Dark Rum and Chocolate Milkshake
layout: recipe
tags:
    - drink
    - desert
thumb:
    link: https://www.flickr.com/photos/windgazer/29216066118
    src: https://farm1.staticflickr.com/833/29216066118_fee05b2dd4_c.jpg
---

So, here I was having a bit with a mate in some burger place in Amsterdam. Now,
you should know, I do not drink beer. It's not because I do not drink alcohol,
it's just beer I don't like :). After picking the burger I went and had a look
at the drinks, lo and behold, a chocolate and rum milkshake!!

And here I was at home, pondering this glorious milkshake, not to be outdone.
In theory it'd be the easiest thing, dark rum, dark chocolate and vanilla
ice-cream. All it should take is a couple of tries to get the mix right, a tough
job but somebody's gotta do it. So I went through those trial and errors, so you
don't have to!

Here's the mix I came up with:

- 2 shots of Dark Rum (Captain Morgan)
- 4 shots of Dark Chocolate Milk (Chocomel Dark)
- 4 large scoops of ice-cream

Here's the recipe:

1. Put all the liquids in the blender
2. Add the ice-cream
3. Run the blender until you have a smooth mixture

The aim is the end up with roughly 250ml worth of milkshake, which is a large
glass full.

[![Dark Rum and Chocolate Milkshake][imgI]][I]

For those of you unlucky enough not to be able to get your hands on on a dark
chocolate milk (which probably means anybody not living in the Netherlands), the
recipe is a little more involved and involves the following:

- 1 shot of hot water
- 2 teaspoons of cacao powder
- 2 shots of dark rum
- 4 shots of milk
- 4 large scoops of ice-cream

Here's the recipe:

1. First mix the water with the cacao and stir to get a smooth mixture without
  lumps.
2. Put together all the liquids in the blender
3. Add the ice-cream
4. Run the blender until you get a smooth mix

[I]: https://www.flickr.com/photos/windgazer/29216066118
[imgI]: https://farm1.staticflickr.com/833/29216066118_fee05b2dd4.jpg
