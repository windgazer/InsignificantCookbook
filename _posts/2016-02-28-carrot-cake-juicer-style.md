---
title: Carrot Cake, juicer style
layout: recipe
tags:
    - cake
    - desert
thumb:
    link: https://www.flickr.com/photos/windgazer/25226004232
    src: https://farm2.staticflickr.com/1534/25226004232_175ea98639_b.jpg
---

There's just some of these foods that are not often found in the Netherlands. Cheesecake
for one, carrot-cake is another. I had, long ago, taken the effort of making carrot-cake
and vividly recall the effort it took to grate the carrots.

Some time ago, I think about a year actually, I started using a juicer. Not so much for
the concept of 'micro-nutrients', rather for the speed at which I can produce a healthy
meal as compared to throwing a pizza in the oven (the unhealthy fast alternative). The
bonus of juicing being the speed at which my body can process it, making it the perfect
food-source when I want to go exercise after work.

Now, why did I start talking about juicing? Weren't we discussing carrot-cake? Well, the
by-product of juicing is a pulp, not unlike a slightly moist flour. I saw a way to make me
some nice carrot-cake without the arduous task of grating the carrots!

- 4 eggs
- 3 cups juiced carrot-pulp (takes about a bunch of carrots, which is some 500-600 grams).
- 2 cups of flour
- 2 cups of sugar (I recommend cane-sugar)
- 1.25 cups of olive oil
- baking soda, roughly a teaspoon or two
- baking powder, the same
- pinch of salt
- 2 teaspoons of vanilla sugar
- Optionally add a liberal dose of cinnamon

To prepare just throw together all but the flour and carrot, mix until smooth, then throw
in the rest. Not so scientific, but it's the result that counts! In the oven for 55min at
180&#8451;. To make sure, do the toothpick test (stick it in, take it out, should still
be clean...).

[![Carrot Cake][imgI]][I]

For frosting, google on my friend. I've not perfected the frosting, and I'm still looking
for one that isn't so insanely sweet that you don't even taste the carrot anymore.

[I]: https://www.flickr.com/photos/windgazer/25226004232
[imgI]: https://farm2.staticflickr.com/1534/25226004232_175ea98639.jpg
